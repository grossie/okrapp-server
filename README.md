API:
    Postgraphile
        npx postgraphile -c postgres://user:pass@okr-db/okr_dev -j -n 0.0.0.0 --cors
    Postgres
        CREATE TABLE users (id serial PRIMARY KEY, username citext NOT NULL unique, first_name text, last_name text NOT NULL, is_managment boolean NOT NULL DEFAULT false, is_admin boolean NOT NULL DEFAULT false, created_at timestamptz NOT NULL DEFAULT now(), updated_at timestamptz NOT NULL DEFAULT now());

        CREATE TABLE missions (id serial PRIMARY KEY, name text NOT NULL unique);

        CREATE TABLE objectives (id serial PRIMARY KEY, description text NOT NULL unique, year int NOT NULL, quarter int NOT NULL, mission_id integer REFERENCES missions (id),  grade real NOT NULL);

        CREATE TABLE key_results (id serial PRIMARY KEY, description text NOT NULL unique, objective_id integer REFERENCES objectives (id), grade real NOT NULL, start_date timestamptz, due_date timestamptz, kr_measurable text, notes text);

        CREATE TABLE assignemnts (id serial PRIMARY KEY, user_id integer REFERENCES users (id), key_result_id integer REFERENCES key_results (id), type char NOT NULL);