const postgraphql = require(`postgraphql`).postgraphql
const { graphqlExpress, graphiqlExpress } = require(`apollo-server-express`)
const schema  = require('./db/schema')

app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }))
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }))

app.use(postgraphql('postgres://localhost:5432', 'public', {graphiql: true}))

